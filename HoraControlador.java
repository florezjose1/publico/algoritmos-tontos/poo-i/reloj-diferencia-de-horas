/**
 * Write a description of class HoraControlador here.
 *
 * @author (Jose Florez - florezjoserodolfo@gmail.com)
 * @version (a version number or a date)
 */
/**
 * Sample Skeleton for 'Hora.fxml' Controller Class
 */

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class HoraControlador {

    @FXML // fx:id="btnMoverHora1"
    private Button btnMoverHora1; // Value injected by FXMLLoader

    @FXML // fx:id="btnMoverMinutero1"
    private Button btnMoverMinutero1; // Value injected by FXMLLoader

    @FXML // fx:id="btnMoverSegundero1"
    private Button btnMoverSegundero1; // Value injected by FXMLLoader

    @FXML // fx:id="btnCambiarFormato1"
    private Button btnCambiarFormato1; // Value injected by FXMLLoader

    @FXML // fx:id="txtHora1"
    private Label txtHora1; // Value injected by FXMLLoader

    @FXML // fx:id="btnMoverHora2"
    private Button btnMoverHora2; // Value injected by FXMLLoader

    @FXML // fx:id="btnMoverMinutero2"
    private Button btnMoverMinutero2; // Value injected by FXMLLoader

    @FXML // fx:id="btnMoverSegundero2"
    private Button btnMoverSegundero2; // Value injected by FXMLLoader

    @FXML // fx:id="btnCambiarFormato2"
    private Button btnCambiarFormato2; // Value injected by FXMLLoader

    @FXML // fx:id="txtHora2"
    private Label txtHora2; // Value injected by FXMLLoader

    @FXML // fx:id="txtDiferenciaHora"
    private Label txtDiferenciaHora; // Value injected by FXMLLoader

    private Hora hora1;
    private Hora hora2;
    
    public HoraControlador() {
        hora1 = new Hora(0,0,0);
        hora2 = new Hora(0,0,0);
    }

    @FXML
    void cambiarFormato1() {
        if(hora1.getFormato() == hora1.HH12){
            hora1.setFormato(hora1.HH24);
        } else if(hora1.getFormato() == hora1.HH24){
            hora1.setFormato(hora1.HH12);
        }
        this.stringHora1();
    }

    @FXML
    void cambiarFormato2() {
        if(hora2.getFormato() == hora2.HH12){
            hora2.setFormato(hora2.HH24);
        } else if(hora2.getFormato() == hora2.HH24){
            hora2.setFormato(hora2.HH12);
        }
        this.stringHora2();
    }
    
    private void stringHora1() {
        txtHora1.setText(hora1.toString());
        this.restarHoras();
    }
    
    private void stringHora2() {
        txtHora2.setText(hora2.toString());
        this.restarHoras();
    }
    
    private void restarHoras() {
        txtDiferenciaHora.setText("Diferencia : " + hora1.restar(hora2) + " segundos");
    }

    @FXML
    void moverHora1() {
        hora1.moverHorario();
        this.stringHora1();
    }

    @FXML
    void moverHora2() {
        hora2.moverHorario();
        this.stringHora2();
    }

    @FXML
    void moverMinutero1() {
        hora1.moverMinutero();
        this.stringHora1();
    }

    @FXML
    void moverMinutero2() {
        hora2.moverMinutero();
        this.stringHora2();
    }

    @FXML
    void moverSegundero1() {
        hora1.moverSegundero();
        this.stringHora1();
    }

    @FXML
    void moverSegundero2() {
        hora2.moverSegundero();
        this.stringHora2();
    }

    @FXML
    void initialize() {
        assert btnMoverHora1 != null : "fx:id=\"btnMoverHora1\" was not injected: check your FXML file 'HoraVista.fxml'.";
        assert btnMoverMinutero1 != null : "fx:id=\"btnMoverMinutero1\" was not injected: check your FXML file 'HoraVista.fxml'.";
        assert btnMoverSegundero1 != null : "fx:id=\"btnMoverSegundero1\" was not injected: check your FXML file 'HoraVista.fxml'.";
        assert btnCambiarFormato1 != null : "fx:id=\"btnCambiarFormato1\" was not injected: check your FXML file 'HoraVista.fxml'.";
        assert txtHora1 != null : "fx:id=\"txtHora1\" was not injected: check your FXML file 'HoraVista.fxml'.";
        assert btnMoverHora2 != null : "fx:id=\"btnMoverHora2\" was not injected: check your FXML file 'HoraVista.fxml'.";
        assert btnMoverMinutero2 != null : "fx:id=\"btnMoverMinutero2\" was not injected: check your FXML file 'HoraVista.fxml'.";
        assert btnMoverSegundero2 != null : "fx:id=\"btnMoverSegundero2\" was not injected: check your FXML file 'HoraVista.fxml'.";
        assert btnCambiarFormato2 != null : "fx:id=\"btnCambiarFormato2\" was not injected: check your FXML file 'HoraVista.fxml'.";
        assert txtHora2 != null : "fx:id=\"txtHora2\" was not injected: check your FXML file 'HoraVista.fxml'.";
        assert txtDiferenciaHora != null : "fx:id=\"txtDiferenciaHora\" was not injected: check your FXML file 'HoraVista.fxml'.";
        
        this.stringHora1();
        this.stringHora2();
    }

}
