### Reloj

Se quiere desarrollar la clase Hora, según las siguientes reglas:

1) La clase puede tener dos formatos:

  a) HH24 desde 00:00:00 hasta 23:59:59

  b) HH12 desde 12:00:00 a.m.  pasando por 12:00:00 p.m. hasta 11:59:59 p.m.

2) Deben poderse comparar dos horas para saber sin son iguales o una mayor y otra menor.

3) Se debe poder convertir el formato de una hora. Si está en formato HH24 pasar a HH12. Por ejemplo 15:01:02 es 03:01:02 p.m. o al revés.

4) Se pueden restar dos horas y dar el resultado en segundos.

5) Proponga una GUI y el controller en JavaFX para tener una aplicación 100% funcional.

![Img](reloj.png)

Versión Bluej: `4.2.2`

Versión Java: `11.0.6`

Hecho con ♥ por [Jose Florez](https://joseflorez.co)

