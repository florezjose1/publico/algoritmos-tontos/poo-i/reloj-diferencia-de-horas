/**
 * Clase Hora
 * 
 * @author (Milton Jesús Vera Contreras miltonjesusvc@ufps.edu.co)
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com)
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
public class Hora
{
    /**Constante para el formato de 24 horas*/
    public static final String HH24 = "HH24";

    /**Constante para el formato de 12 horas con a.m. y p.m. */
    public static final String HH12 = "HH12";

    /**Indica el formato de hora HH12 o HH24*/
    protected String formato;

    /**Constante para la jornada entre la media noche y las 11 a.m. */
    public static final String AM = "a.m.";

    /**Constante para la jornada entre medio dia y las 11 p.m. */
    public static final String PM = "p.m.";

    /**Indica la jornada, su valor debe ser Hora.AM u Hora.PM*/
    protected String ampm;

    /**Indica la cantidad de horas de la hora: entre 1 y 12*/
    protected int horas;

    /**Indica la cantidad de minutos de la hora: entre 0 y 59*/
    protected int minutos;

    /**Indica la cantidad de segundos de la hora: entre 0 y 59*/
    protected int segundos;

    /**
     * Constructor 1 - por defecto con la media noche en formato HH12
     */
    public Hora() {
         //COMPLETE
         // this.formato = this.HH12;
         // this.ampm = this.AM;
         // this.horas = 12;
         // this.minutos = 0;
         // this.segundos = 0;
         this(HH12);
    }

    /**
     * Constructor 2 - por defecto con la media noche, segun el formato recibido como parámetro
     * @param formato el formato a usar HH12 o HH24.
     */
    public Hora(String formato) {
        //COMPLETE
        this(0,0,0);
        this.setFormato(formato);
    }

    /**
     * Constructor 3 - con parametros 1. Asume por defecto formato HH24
     * @param horas el valor inicial de las horas.
     * @param minutos el valor inicial de los minutos.
     * @param segundos el valor inicial de los segundos.
     */
    public Hora(int horas, int minutos, int segundos)  {
        //COMPLETE
        this(horas, minutos, segundos, HH24, null);
    }

    /**
     * Constructor 4 - con parametros. Asume por defecto HH12 y recibe la jornada AM/PM.
     * (es recomendable que los otros constructores siempre invoquen este constructor)
     * @param horas el valor inicial de las horas.
     * @param minutos el valor inicial de los minutos.
     * @param segundos el valor inicial de los segundos.
     * @param ampm Determina si la hora es AM o PM
     */
    public Hora(int horas, int minutos, int segundos, String ampm) {
        this(horas, minutos, segundos, HH12, ampm);
    }

    /**
     * Constructor 5 - con parametros.
     * (es recomendable que los otros constructores siempre invoquen este constructor)
     * @param horas el valor inicial de las horas.
     * @param minutos el valor inicial de los minutos.
     * @param segundos el valor inicial de los segundos.
     * @param formato el formato a usar HH12 o HH24.
     * @param ampm aplica sólo cuando formato es HH12 para determinar si la hora es AM o PM.Si formato es HH24 se ignora.
     */
    public Hora(int horas, int minutos, int segundos, String formato, String ampm) {
        //COMPLETE
        this.formato = formato;
        this.horas = horas;
        this.minutos = minutos;
        this.segundos = segundos;
        if(this.getFormato() == this.HH12) this.ampm = ampm;
    }
    
    /**
     * Valida la hora integralmente. 
     * Es usada sólo por los constructores (deber&iacute;a ser private)
     * Se usa protected en lugar de private para las pruebas unitarias
     * @return false cuando los datos son incorrectos para formato, horas, minutos, segundos y ampm.
     */
    protected boolean esHoraValida(){
        boolean valid = false;
        if(this.horas >= 0 && 
           this.minutos >=0 && this.minutos <= 59 && 
           this.segundos >= 0 && this.segundos <= 59) {
           
           if(this.formato == this.HH12 && (this.ampm == this.AM || this.ampm == this.PM)) {
               if(this.horas >= 1 && this.horas <= 12) valid = true;
           } else if(this.formato == this.HH24) {
               if(this.horas < 24) valid = true;
           }    
        }
        return valid;//COMPLETE
    }

    /**
     * Regresa un nuevo objeto Hora en formato HH24 con los mismos datos de this
     * @return un nuevo objeto copia de this en formato HH24
     */
    public Hora getHoraHH24(){
        this.setFormato(this.HH24);
        Hora h = new Hora(this.horas, this.minutos, this.segundos);
        return h;//COMPLETE
    }

    /**
     * Regresa un nuevo objeto Hora en formato HH12 con los mismos datos de this
     * @return un nuevo objeto copia de this en formato HH12
     */
    public Hora getHoraHH12(){
        this.setFormato(this.HH12);
        Hora h = new Hora(this.horas, this.minutos, this.segundos, this.ampm);
        return h;//COMPLETE
    }

    /***
     * Resta this con la hora recibida y regresa la diferencia en segundos.
     * Si this > other regresa un valor positivo con la cantidad de segundos entre this y other.
     * Si this < other regresa un valor negativo con la cantidad de segundos entre this y other.
     * Si this == other regresa un valor cero.
     * @param other Otro objeto tipo Hora, diferente de null.
     */
    public int restar(Hora other){
        int t1 = this.getSegundosTiempo(this);
        int t2 = other.getSegundosTiempo(other);
        return t1 - t2;//COMPLETEreturn 0;//COMPLETE
    }

    /**
     * Averigua si this es igual a other.
     * @param other Otro objeto tipo Hora, diferente de null.
     */
    public boolean esIgual(Hora other){
        int t1 = this.getSegundosTiempo(this);
        int t2 = other.getSegundosTiempo(other);
        return t1 == t2;//COMPLETE
    }
    
    /**
     * Averigua si this es menor que other.
     * @param other Otro objeto tipo Hora, diferente de null.
     */
    public boolean esMenor(Hora other){
        int t1 = this.getSegundosTiempo(this);
        int t2 = other.getSegundosTiempo(other);
        return t1 < t2;//COMPLETE
    }

    /**
     * Averigua si this es mayor que other.
     * @param other Otro objeto tipo Hora, diferente de null.
     */
    public boolean esMayor(Hora other){
        int t1 = this.getSegundosTiempo(this);
        int t2 = other.getSegundosTiempo(other);
        return t1 > t2;//COMPLETE
    }
    
    /**
     * Convierte la hora dada en segundos para que en la comparación sea mas fácil hacerse
     */
    private int getSegundosTiempo(Hora obj) {
        int h = obj.getHoras();
        int m = obj.getMinutos();
        int s = obj.getSegundos();
        
        if(obj.getFormato() == obj.HH12) {
            if(obj.getAmpm() == obj.AM && h == 12) {
                h = 0;
            } else if(obj.getAmpm() == obj.PM && h < 12) {
                h += 12;
            }
        }
        
        h = h * 60 * 60;
        m = m * 60;
        return h + m + s;
    }

    /**
     * Regresa un String que representa esta Hora
     * @return string de la hora, para horas, minutos y segundos menores de 10, se les agrega un 0, 
     * para que queden 00:00:00 y no 0:0:0
     */
    public String toString(){
        String str = "";
        int h = this.getHoras();
        if(h < 10)
            str += "0" + h + " : ";
        else
            str += h + " : ";
        
        int m = this.getMinutos();
        if(m<10)
            str += "0" + m + " : ";
        else
            str += m + " : ";
        
        int s = this.getSegundos();
        if(s<10)
            str += "0" + s;
        else
            str += s;
        
        String format = "";
        if(this.getFormato() == this.HH12)
            format = " " + this.getAmpm();
        
        return str + format;//COMPLETE
    }
    
    /**
     * Agrego nuevos métodos para realizar la interfaz con botones en donde las horas no sea escritas sino actualizadas 
     * mediantes botones
    */
   
    /**
     * Metodo para mover le segundero
     */
    public void moverSegundero() {
        //COMPLETE EL ALGORITMO
        if (this.getSegundos() == 59) {
            this.segundos = 0;
            this.moverMinutero();
        } else {
            this.segundos++;
        }

    }

    /**
     * Metodo para mover le minutero
     */
    public void moverMinutero() {
        //COMPLETE EL ALGORITMO
        if (this.getMinutos() == 59) {
            this.minutos = 0;
            this.moverHorario();
        } else {
            this.minutos++;
        }
    }
    
    /**
     * Metodo para mover le horario
     */
    public void moverHorario() {
        //COMPLETE EL ALGORITMO
        if(this.formato == this.HH12){
            if (this.horas == 12) {
                this.horas = 1;
            } else {
                this.horas++;
                if (this.horas == 12) {
                    if(this.ampm == this.AM){
                        this.setAmpm(this.PM);
                    } else {
                        this.setAmpm(this.AM);
                    }
                }
            }
        } else {
            if (this.getHoras() == 23) {
                this.horas = 0;
            } else {
                this.horas++;
            }
        }
    }
    
    /**
     * Setter method formato
     * A diferencia de los otros GET/SET setFormato es muy importante y se sugiere
     * implementarlo...
     * Se encarga de convertir el formato HH12 a HH24 o HH24 a HH12
     * Ejemplos:
     * Si this es 00:05:06 y formato es HH12 debe retornar 12:05:06 a.m. en formato HH12
     * Si this es 12:01:02 a.m. y formato es HH24 debe retornar 00:01:02 en formato HH24
     * @param formato Puede ser HH24 o HH12
    */
    public void setFormato(String formato){
        //COMPLETE
        if(this.formato == this.HH12 && formato == this.HH24) {
            int h = this.getHoras(); // 03 PM - 10 AM - 12AM - 12PM
            if(h == 12 && this.ampm == this.AM){ // == 12 AM
                this.setHoras(0); // Las 12AM en formato HH12 son las 00 en formato HH24
            } else if(this.ampm == this.PM && h < 12) { // == 03PM
                h += 12; // 03 + 12 = 15 Formato HH24
                this.setHoras(h); 
            }
            this.setAmpm(null);
        } else if(this.formato == this.HH24 && formato == this.HH12) {
            int h = this.getHoras(); // 20 - 10 - 0
            if(h == 0){ // == 0
                this.setHoras(12); // las 0 en formato HH24 son las 12 AM en formato HH12
                this.setAmpm(this.AM);
            } else if(h < 12) {
                this.setAmpm(this.AM);
            } else { // 20 > 12
                if(h > 12) this.setHoras(h - 12); // 20 - 12 = 8 PM
                
                this.setAmpm(this.PM);
            }
        }
        this.formato = formato;
    }//end method setFormato

    /**Metodo de acceso a la propiedad ampm*/
    public String getAmpm(){
        return this.ampm;
    }//end method getAmpm

    /**Metodo de modificación a la propiedad ampm*/
    public void setAmpm(String ampm){
        this.ampm = ampm;
    }//end method setAmpm

    /**Metodo de acceso a la propiedad segundos*/
    public int getSegundos(){
        return this.segundos;
    }//end method getSegundos

    /**Metodo de modificación a la propiedad segundos*/
    public void setSegundos(int segundos){
        this.segundos = segundos;
    }//end method setSegundos

    /**Metodo de acceso a la propiedad minutos*/
    public int getMinutos(){
        return this.minutos;
    }//end method getMinutos

    /**Metodo de modificación a la propiedad minutos*/
    public void setMinutos(int minutos){
        this.minutos = minutos;
    }//end method setMinutos

    /**Metodo de acceso a la propiedad horas*/
    public int getHoras(){
        return this.horas;
    }//end method getHoras

    /**Metodo de modificación a la propiedad horas*/
    public void setHoras(int horas){
        this.horas = horas;
    }//end method setHoras
    
    /**Getter method formato*/
    public String getFormato(){
        return this.formato;
    }//end method getFormato
    
}